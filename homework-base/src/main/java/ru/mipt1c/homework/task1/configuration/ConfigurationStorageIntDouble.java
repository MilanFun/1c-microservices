package ru.mipt1c.homework.task1.configuration;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration(value = "conf2")
@EnableKafka
@ComponentScan("ru.mipt1c.homework.task1")
@Import(value = TopicKafkaConfiguration.class)
public class ConfigurationStorageIntDouble {
    @Bean
    public Map<String, Object> producerPropertiesIntDouble() {
        Map<String, Object> properties = new HashMap<>();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, DoubleSerializer.class);
        return properties;
    }

    @Bean
    public ProducerFactory<Integer, Double> producerFactoryIntDouble() {
        return new DefaultKafkaProducerFactory<>(producerPropertiesIntDouble());
    }

    @Bean
    public KafkaTemplate<Integer, Double> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactoryIntDouble());
    }

    @Bean
    public Consumer<Integer, Double> consumer() {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "storage-consumer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, DoubleDeserializer.class.getName());

        final Consumer<Integer, Double> consumer = new KafkaConsumer<>(props);

        consumer.subscribe(Arrays.asList(topic()));
        return consumer;
    }

    @Bean
    public String topic() {
        return "storage-topic-int-double";
    }
}
