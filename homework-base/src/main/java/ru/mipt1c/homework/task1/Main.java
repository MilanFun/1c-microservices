package ru.mipt1c.homework.task1;

import org.apache.kafka.clients.consumer.Consumer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.kafka.core.KafkaTemplate;
import ru.mipt1c.homework.task1.configuration.ConfigurationStorageStrStr;

import java.io.IOException;

public class Main {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
//        AnnotationConfigApplicationContext context =
//                AnnotationConfigApplicationContext(ConfigurationStorageIntDouble.class);
        AnnotationConfigApplicationContext context = new
                AnnotationConfigApplicationContext(ConfigurationStorageStrStr.class);
        Consumer<String, String> consumer = (Consumer<String, String>) context.getBean("consumer");
        KafkaTemplate<String, String> kafkaTemplate = (KafkaTemplate<String, String>) context.getBean("kafkaTemplate");
        String topic = (String) context.getBean("topic");
        try {
            KafkaKeyValueStorageImpl<String, String> k =
                    (KafkaKeyValueStorageImpl<String, String>) context.getBean("kafkaStorage",
                            consumer, kafkaTemplate, topic);
//            KafkaKeyValueStorageImpl<Integer, Double> k =
//                    (KafkaKeyValueStorageImpl<Integer, Double>) context.getBean("kafkaStorage",
//                            consumer, kafkaTemplate, topic);
//            k.write("Test1", "Test1");
//            k.write("Test2", "Test2");
//            k.write("Test31", "Test3");
//            k.write(1, 2.0);
//            k.write(2, 3.12);
            System.out.println(k.size());
            k.readKeys().forEachRemaining(System.out::println);
            k.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        context.close();
    }
}
