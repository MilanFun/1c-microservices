package ru.mipt1c.homework.task1;

import org.apache.kafka.clients.consumer.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component("kafkaStorage")
public class KafkaKeyValueStorageImpl<K, V> implements KeyValueStorage<K, V> {
    private KafkaTemplate<K, V> kafkaTemplate;
    private Map<K, V> cache = new ConcurrentHashMap<>();
    private boolean closed = false;
    public final String topic;

    @Autowired
    public KafkaKeyValueStorageImpl(Consumer<K, V> consumer, KafkaTemplate<K, V> kafkaTemplate, String topic) {
        this.topic = topic;
        this.kafkaTemplate = kafkaTemplate;
        final int giveUp = 100;
        int noRecordsCount = 0;
        while (true) {
            final ConsumerRecords<K, V> consumerRecords = consumer.poll(100L);
            if (consumerRecords.count() == 0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) {
                    break;
                } else {
                    continue;
                }
            }

            consumerRecords.forEach(record -> {
                this.cache.put(record.key(), record.value());
            });

            consumer.commitAsync();
        }
        consumer.close();
        System.out.println("DONE");
    }

    @Override
    public V read(K key) {
        if (!closed) {
            return this.cache.get(key);
        } else {
            throw new ClosedStorageException("Storage is already closed");
        }
    }

    @Override
    public boolean exists(K key) {
        if (!closed) {
            return this.cache.containsKey(key);
        } else {
            throw new ClosedStorageException("Storage is already closed");
        }
    }

    @Override
    public void write(K key, V value) {
        if (!closed) {
            this.cache.put(key, value);
        } else {
            throw new ClosedStorageException("Storage is already closed");
        }
    }

    @Override
    public void delete(K key) {
        if (!closed) {
            this.cache.remove(key);
        } else {
            throw new ClosedStorageException("Storage is already closed");
        }
    }

    @Override
    public Iterator<K> readKeys() {
        if (!closed) {
            return this.cache.keySet().iterator();
        } else {
            throw new ClosedStorageException("Storage is already closed");
        }
    }

    @Override
    public int size() {
        if (!closed) {
            return this.cache.size();
        } else {
            throw new ClosedStorageException("Storage is already closed");
        }
    }

    @Override
    public void flush() {
        if (!closed) {
            for (Map.Entry<K, V> entry : this.cache.entrySet()) {
                this.kafkaTemplate.send(topic, entry.getKey(), entry.getValue());
            }
        } else {
            throw new ClosedStorageException("Storage is already closed");
        }
    }

    @Override
    public void close() throws IOException {
        flush();
        this.closed = true;
    }
}
